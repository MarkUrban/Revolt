﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevoltWebApp.Data.Migrations
{
    public partial class AddingActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "62b1d7e2-ec4d-4211-b795-944a27ab2f93");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "ba75705f-6004-4460-a013-ec0148687dfb");

            migrationBuilder.CreateTable(
                name: "Activity",
                columns: table => new
                {
                    ActivityId = table.Column<string>(nullable: false),
                    Time = table.Column<string>(nullable: true),
                    ApplicationUserID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activity", x => x.ActivityId);
                    table.ForeignKey(
                        name: "FK_Activity_AspNetUsers_ApplicationUserID",
                        column: x => x.ApplicationUserID,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "30ed0c13-85a2-4f0e-a0e1-aea96bd63019", 0, "228b1c8a-e5e3-482b-a1d1-3ae01cb26b02", "public@flex-cast.cz", true, null, null, false, null, "PUBLIC@FLEX-CAST.CZ", "ADMIN", "AQAAAAEAACcQAAAAEEXYJDvPtEvChw2wnTi7z0B1p8LpplU0WB6zhzpfKLZFYhWKcp4mvBhQexJRK5wxtQ==", null, false, "c77ca217-f3f9-4b71-8c6a-62db92ca667a", false, "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "3076660d-5d2f-4956-9f3c-9c08206087fe", 0, "84353eef-8d9d-44bc-ac60-b83e894bfe31", "marek@flex-cast.cz", true, null, null, false, null, "MAREK@FLEX-CAST.CZ", "MANAGER", "AQAAAAEAACcQAAAAEGVXlFeevrYFtQiSyUQvDyLYTTH5H7O5d/tzG2oyzcNo/iCTW7n2bqXLYj6qSPBRsg==", null, false, "33a98f56-a740-4078-9104-1307314f9076", false, "Manager" });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_ApplicationUserID",
                table: "Activity",
                column: "ApplicationUserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Activity");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "3076660d-5d2f-4956-9f3c-9c08206087fe");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "30ed0c13-85a2-4f0e-a0e1-aea96bd63019");

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "ba75705f-6004-4460-a013-ec0148687dfb", 0, "397721bb-e03d-471a-9882-fb7d87a1639c", "public@flex-cast.cz", true, null, null, false, null, "PUBLIC@FLEX-CAST.CZ", "ADMIN", "AQAAAAEAACcQAAAAEPAk5zikA8DgoXIPtKSR2/FbqFkEVeAldvUHScIUw3PCp2Q56EDec2GRK3xr66pnhw==", null, false, "b265e820-4e00-441e-8bcb-6e6b713a1084", false, "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "62b1d7e2-ec4d-4211-b795-944a27ab2f93", 0, "f8206fea-0b65-4282-9b7d-f0374f17b953", "marek@flex-cast.cz", true, null, null, false, null, "MAREK@FLEX-CAST.CZ", "MANAGER", "AQAAAAEAACcQAAAAEBlykJRHMaDOI9H2fbTMJN+ySMgpaYOubAc964px91OuQiR0td9QKMrwo/PfG9wtlA==", null, false, "86bb47bb-37c1-4eb2-b7af-fe27f97e4d1e", false, "Manager" });
        }
    }
}
