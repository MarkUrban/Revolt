﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RevoltWebApp.Data.Migrations
{
    public partial class SeedUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "ba75705f-6004-4460-a013-ec0148687dfb", 0, "397721bb-e03d-471a-9882-fb7d87a1639c", "public@flex-cast.cz", true, null, null, false, null, "PUBLIC@FLEX-CAST.CZ", "ADMIN", "AQAAAAEAACcQAAAAEPAk5zikA8DgoXIPtKSR2/FbqFkEVeAldvUHScIUw3PCp2Q56EDec2GRK3xr66pnhw==", null, false, "b265e820-4e00-441e-8bcb-6e6b713a1084", false, "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "ID1", "ID2", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "62b1d7e2-ec4d-4211-b795-944a27ab2f93", 0, "f8206fea-0b65-4282-9b7d-f0374f17b953", "marek@flex-cast.cz", true, null, null, false, null, "MAREK@FLEX-CAST.CZ", "MANAGER", "AQAAAAEAACcQAAAAEBlykJRHMaDOI9H2fbTMJN+ySMgpaYOubAc964px91OuQiR0td9QKMrwo/PfG9wtlA==", null, false, "86bb47bb-37c1-4eb2-b7af-fe27f97e4d1e", false, "Manager" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "62b1d7e2-ec4d-4211-b795-944a27ab2f93");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "ba75705f-6004-4460-a013-ec0148687dfb");
        }
    }
}
