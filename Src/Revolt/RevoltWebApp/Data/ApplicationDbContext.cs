﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RevoltWebApp.Models;

namespace RevoltWebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var adminUser = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Admin",
                NormalizedUserName = "Admin".ToUpper(),
                Email = "public@flex-cast.cz",
                NormalizedEmail = "public@flex-cast.cz".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var managerUser = new ApplicationUser
            {
                Id = Guid.NewGuid().ToString(),
                UserName = "Manager",
                NormalizedUserName = "Manager".ToUpper(),
                Email = "marek@flex-cast.cz",
                NormalizedEmail = "marek@flex-cast.cz".ToUpper(),
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var hashGenerator = new PasswordHasher<ApplicationUser>();
            adminUser.PasswordHash = hashGenerator.HashPassword(adminUser, "Admin12.");
            managerUser.PasswordHash = hashGenerator.HashPassword(managerUser, "Manager12.");

            builder.Entity<ApplicationUser>().HasData(
                    adminUser,
                    managerUser
                );

        }
    }
}
