﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RevoltWebApp.Models;

namespace RevoltWebApp.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class SendController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;

        public SendController(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }
        // GET: api/Generate
        [HttpGet]
        public bool Get()
        {
            var users = userManager.Users;
            Parallel.ForEach(users, user =>
            {
                SendEmail(user);
            });
            return true;
        }

        void SendEmail(ApplicationUser user)
        {
            string emailAddress = user.Email;

            // TODO:
            // Some kind of SMTP magic goes here
        }

    }
}
