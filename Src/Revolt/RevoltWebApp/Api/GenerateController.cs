﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using RevoltWebApp.Models;

namespace RevoltWebApp.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenerateController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly WordFactory wordFactory;

        public GenerateController(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
            this.wordFactory = new WordFactory();

        }
        // GET: api/Generate
        [HttpGet]
        public async Task<bool> Get()
        {
            IList<ApplicationUser> users = userManager.Users.ToList();
            wordFactory.GenerateWords((uint)users.Count * (uint)2);

            foreach(var user in users)
            {
                user.ID1 = wordFactory.Words.Pop();
                user.ID2 = wordFactory.Words.Pop();
                await userManager.UpdateAsync(user);
            }

            return true;
        }



    }
}
