﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RevoltWebApp.Models
{
    public class Activity
    {
        public string ActivityId { get; set; }
        public string Time { get; set; }
        public string ApplicationUserID { get; set; }
    }
}
