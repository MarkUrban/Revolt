﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RevoltWebApp.Models
{
    public class WordFactory
    {


        public Stack<string> Words { private set;  get; }

        public WordFactory()
        {
            Words = new Stack<string>();
        }

        public void GenerateWords(uint count)
        {
            WebClient client = new WebClient();
            string rawJSON = client.DownloadString("https://random-word-api.herokuapp.com/word?key=8E2EZH99&number=" + count.ToString());
            rawJSON = rawJSON.Trim('[', ']');
            string[] wordsRaw = rawJSON.Split(',');

            Words.Clear();
            foreach (string word in wordsRaw)
            {
                Words.Push(word.Substring(2, word.Length - 3));
            }

        }
    }
}
